# [WIP] College Docker

This project will set a docker structure for college app.

#### Project Goals:
After `docker-compose up` command this will run should run college app.

#### Project Structure:

- ./docker-compose.yml - main docker compose  file.
- ./.env - file for environment variables to use on containers configs.
- ./docker_services - this folder contains all docker compose services config and docker files.


#### Instructions:

- Clone this repo `git clone https://gitlab.com/slipnox/docker-college.git`.
- `cd docker-college` and then clone college project repo like this: `git clone https://gitlab.com/katanateam/college.git college_app`, [college_app] name is important at least for now.
- `docker-compose up` and after loads enter to http[s]://college.test

#### TODO's

* Load project with nginx.
* To make Laravel 4 load .env file variables to combine with main project .env file.
* Add ssl server on nginx config.
* Ensure memcached and redis are working as it should.
